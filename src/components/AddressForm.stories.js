import AddressForm from './AddressForm.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'components/AddressForm',
  component: AddressForm,
  argTypes: {
    onClose: {},
    'onUpdate:addressToEdit': {},
  },
};

const fields = [{
  name: 'road',
  label: 'Road',
  title: 'Road',
  type: 'text',
  style: {
    color: 'red'
  }
}, {
  name: 'zipCode',
  label: 'Zip Code',
  title: 'Zip Code',
  type: 'number',
  style: {
    backgroundColor: 'green'
  }
}, {
  name: 'city',
  label: 'City',
  title: 'City',
  type: 'text'
}, {
  name: 'complement',
  label: 'Complement',
  title: 'Complement',
  type: 'text'
}, {
  name: 'country',
  label: 'Country',
  title: 'Country',
  type: 'text'
}, {
  name: 'state',
  label: 'State',
  title: 'State',
  type: 'text'
}]
// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { AddressForm },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<AddressForm v-bind="args" />'
});

/**
 * Default story, without prop, need to work
 */
export const Default = Template.bind({});


/**
 * With an AddressForm in prop
 */
export const WithProp = Template.bind({});
WithProp.args = {
  addressToEdit: {
    title: 'My address',
    city: 'Nantes',
    zipCode: '44000',
    road: 'rue du Marchix',
    state: 'France'
  },
  fields
};
