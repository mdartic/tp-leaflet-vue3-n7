// .storybook/preview.js
import { app } from '@storybook/vue3';
import { createI18n } from 'vue-i18n'
import fr from '../src/lang/fr_FR'
import en from '../src/lang/en_EN'

const i18n = createI18n({
  // something vue-i18n options here ...
  locale: 'fr',
  messages: {
    en: en,
    fr: fr
  }
})

//👇 Storybook Vue app being extended and registering the library
app.use(i18n)

export const parameters = {
  actions: { argTypesRegex: "^on.*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
