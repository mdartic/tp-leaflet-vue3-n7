// https://docs.cypress.io/api/introduction/api.html

const BASE_URL = 'http://localhost:8080'

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit(BASE_URL + '/')
    cy.contains('h1', 'Welcome to Your Vue.js App')
  })
  it('Visits the about page', () => {
    cy.visit(BASE_URL + '/about')
    cy.contains('h1', 'This is an about page')
  })
  it('Visits the search page', () => {
    cy.visit(BASE_URL + '/search')
    // ouvrir le formulaire de modification de l'adresse destination
    cy.get('button')
      .contains('Modifier Adresse de destination')
      .click()

    // modifier la ville / code postal
    cy.get('input:nth-of-type(2)')
      .clear()
      .type('31870')
    cy.get('input:nth-of-type(3)')
      .clear()
      .type('Lagardelle')

    // fermer le formulaire
    cy.get('button')
      .contains('Close')
      .click()

    // lancer la recherche
    cy.get('button')
      .contains('Chercher Adresse de destination')
      .click()

    cy.wait(500)

    // vérifier que au survol de une ligne du tableau de résultat
    // la popup de la carte se met bien à jour
    // cy.get('button')
    //   .contains('sélect')
    //   .click()

    cy.get('tr')
      .contains('Lagardelle')
      .trigger('mouseover')

    cy.get('#popup-map')
      .contains('Lagardelle')
  })
})
